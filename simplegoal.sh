#!/bin/bash
/usr/bin/python /home/pi/simplegoal/simplegoal.py 2>&1 > /home/pi/simplegoal/logs/`date "+%Y-%m-%dT%H:%M:%S"`.log
/usr/bin/find /home/pi/simplegoal/logs/*.log -mtime +7 -type f -delete
