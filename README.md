Sets a specified Simple goal to Chase credit card balance so that Safe to Spend will always reflect your Simple account balance minus current Chase credit card balance.

Will automatically detect when a credit card payment occurs and assign the payment to the Simple goal.

Requires ofxclient - [https://pypi.python.org/pypi/ofxclient/](https://pypi.python.org/pypi/ofxclient/)