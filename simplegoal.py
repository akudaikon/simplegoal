from ofxclient import Institution
import os
import sys
import requests
import json
import time
import decimal

# Get full paths to files, otherwise cron gets unhappy
config_path = os.path.join(os.path.dirname(__file__), "config.json")
data_path = os.path.join(os.path.dirname(__file__), "data.json")

try:
  with open(config_path, 'r') as config_file:
    config = json.load(config_file)
    config_file.close()
except Exception as e:
  print "Error reading config.json: ", e
  sys.exit(1)

try:
  with open(data_path, 'r') as data_file:
    data = json.load(data_file)
    data_file.close();
except Exception as e:
  data = {}
  print "Error reading data.json: ", e
  print "Will try creating a new one instead..."

inst = Institution(
  id = '10898',
  org = 'B1',
  url = 'https://ofx.chase.com',
  username = config['ChaseAccount']['username'],
  password = config['ChaseAccount']['password'],
  client_args = {
    'ofx_version' : 103,
    'id' : config['ChaseAccount']['uuid']
  }
)

# Get Chase account info via OFX
statement = None
print "Getting account info from Chase..."
try:
  accounts = inst.accounts()
  for account in accounts:
    if account.number == config['ChaseAccount']['account number']:
      statement = account.statement(0)

  if statement is None: raise Exception("account not found")
except Exception as e:
  print "Error getting Chase account info: ", e
  sys.exit(1)

# Get last balance
if (data.has_key('last balance')):
  last_bal = data['last balance']
else:
  last_bal = 0

# Get last running balance
if (data.has_key('running balance')):
  running_bal = data['running balance']
else:
  running_bal = 0

cc_bal = decimal.Decimal(config['ChaseAccount']['total credit']) - statement.available_balance
bal_diff = cc_bal - decimal.Decimal(last_bal)
if (bal_diff < 0): bal_diff = 0
running_bal = decimal.Decimal(running_bal) + bal_diff

print "Last balance: " + str(last_bal)
print "Current balance: " + str(cc_bal)
print "Balance difference: " + str(bal_diff)
print "Running balance: " + str(running_bal)

# Log into Simple and update goal amount only if there's a balance difference
if bal_diff > 0: 
  with requests.Session() as r:
    HEADERS = {
      "Accept-Language": "en-us,en;q=0.5",
      "Accept-Charset": "utf-8",
      "Connection": "keep-alive",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36"
    }
    
    # Grab csrf value from signin page
    try:
      resp = r.get('https://bank.simple.com/signin', headers = HEADERS)
      if resp.status_code != 200: raise Exception("HTTP status code: " + str(resp.status_code))
      s = resp.text.index('meta name="_csrf" content="')
      s += len('meta name="_csrf" content="')
      e = resp.text.index('"', s)
      csrf = resp.text[s:e]
      print "Got Simple CSRF: " + csrf
    except Exception as e:
      print "Error getting Simple CSRF: ", e
      sys.exit(1)

    HEADERS = {
      "Accept-Language": "en-us,en;q=0.5",
      "Accept-Charset": "utf-8",
      "Connection": "keep-alive",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36",
      "Host": "bank.simple.com",
      "Origin": "https://bank.simple.com",
      "Referer": "https://bank.simple.com/signin"
    }

    # Sign in
    print "Signing into Simple..."
    try:
      formdata = {'username' : config['SimpleAccount']['username'],
                  'password' : config['SimpleAccount']['password'],
                  '_csrf'    : csrf
      }
      resp = r.post('https://bank.simple.com/signin', data = formdata, headers = HEADERS)
      if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
      if "Your username and passphrase don't match" in resp.content: raise Exception("invalid username/pasword")
    except Exception as e:
      print "Error signing into Simple: ", e
      sys.exit(1)

    HEADERS = {
      "Accept-Language": "en-us,en;q=0.5",
      "Accept-Charset": "utf-8",
      "Connection": "keep-alive",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36",
      "Host": "bank.simple.com",
      "Origin": "https://bank.simple.com",
      "Referer": "https://bank.simple.com/goals",
      "X-CSRF-Token": csrf,
      "X-Requested-With": "XMLHttpRequest"
    }

    # Get JSON list of goals
    print "Getting goals from Simple..."
    found_goal = None
    try:
      resp = r.get('https://bank.simple.com/goals/data', headers = HEADERS)
      if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
      goals = resp.json()
    
      for goal in goals:
        if goal['name'] == config['SimpleAccount']['goal name'] and goal['archived'] == False:
          found_goal = goal
          print "Found goal \'" + config['SimpleAccount']['goal name'] + "\' (id: " + found_goal['id'] + ")"

      if found_goal is None: raise Exception("Unable to find goal \'" + config['SimpleAccount']['goal name'] + "\'")
    except Exception as e:
      print "Error getting goals: ", e
      sys.exit(1)

    # Get epoch of last time ran
    if (data.has_key("last run")):
      last_epoch = data['last run']
    else:
      last_epoch = int(round(time.time() * 1000))

    # Get JSON list of transactions since last run
    print "Getting transactions from Simple..."
    transactions = None
    try:
      resp = r.get('https://bank.simple.com/transactions/new_transactions?timestamp=' + str(last_epoch), headers = HEADERS)
      if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
      transactions = resp.json()
    except Exception as e:
      print "Error getting transactions: ", e
      sys.exit(1)

    # Look for credit card payment transaction - if found, assign goal to it
    for transaction in transactions['transactions']:
      if transaction['description'] == config['SimpleAccount']['payment name']:
        print "Credit card payment found!"
        print "Assigning goal \'" + config['SimpleAccount']['goal name'] + "\' to payment transaction..."
        formdata = {'transaction_uuid': transaction['uuid'],
                    '_csrf': csrf
        }
        try:
          resp = r.post('https://bank.simple.com/goals/' + found_goal['id'] + '/associations', data = formdata, headers = HEADERS)
          if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
          running_bal = cc_bal
        except Exception as e:
          print "Error assigning goal: ", e
          sys.exit(1)
        
        # Get an updated JSON list of goals
        found_goal = None
        print "Getting updated goals from Simple..."
        try:
          resp = r.get('https://bank.simple.com/goals/data', headers = HEADERS)
          if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
          goals = resp.json()
        
          for goal in goals:
            if goal['name'] == config['SimpleAccount']['goal name'] and goal['archived'] == False:
              found_goal = goal

          if found_goal is None: raise Exception("Unable to find goal \'" + config['SimpleAccount']['goal name'] + "\'")
        except Exception as e:
          print "Error getting goals: ", e
          sys.exit(1)

    # Modify goal amount
    try:
      print "Updating goal \'" + config['SimpleAccount']['goal name'] + "\' to $" + str(running_bal) + "..."
      formdata = {'amount': int((running_bal * 10000) - found_goal['contributed_amount']),
                  '_csrf': csrf
      }
      resp = r.post('https://bank.simple.com/goals/' + found_goal['id'] + '/transactions', data = formdata, headers = HEADERS)
      if resp.status_code != 200: raise Exception("HTTP status code " + str(resp.status_code))
    except Exception as e:
      print "Error modifying goal: ", e
      sys.exit(1) 
else:
  print "No balance difference. Nothing to do!"

# Update last run time epoch
data['last run'] = int(round(time.time() * 1000))
# Update last balance
data['last balance'] = str(cc_bal)
# Update running balance
data['running balance'] = str(running_bal)

with open(data_path, 'w') as data_file:
  json.dump(data, data_file)
  data_file.close()

print "Done!"
sys.exit(0)
